<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/'], function () {
    Route::get('/', 'AdminController@index');
    Route::post('/login', 'AdminController@login')->name('login');
    Route::get('/admin', 'AdminController@dashboard');
    Route::group(['prefix' => '/listing'], function () {

        Route::post('/', 'ListingController@store')->name('createList');
        Route::get('/{id}', 'ListingController@destroy')->name('deleteList');
        Route::patch('/update', 'ListingController@update')->name('updateList');
        Route::get('/view/{id}', 'ListingController@view')->name('viewList');

        Route::get('/', 'ListingController@show');
    });
});
