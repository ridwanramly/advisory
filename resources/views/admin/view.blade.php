@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						Add New Listing
					</div>
					<div class="card-body">
						@if(session('status'))
							<div class="alert alert-success">
								{{session('status')}}
							</div>
						@endif
						<form method="post" action="{{route('updateList')}}">
							 {!! method_field('patch') !!}
							 @csrf
							<div class="form-group">
								<label>List Name</label>
								<input type="text" name="list_name" value="{{$listing->list_name}}" class="form-control">
								<input type="hidden" name="id" value="{{$listing->id}}">
							</div>
							<div class="form-group">
								<label>Distance</label>
								<input type="text" name="distance" value="{{$listing->distance}}" class="form-control">
							</div>
							<div class="form-group">
								<label>User</label>
								<select class="form-control" name="user" value="{{$listing->user_id}}">
									@foreach($users as $user)
										<option value="{{$user->id}}">{{$user->email}}</option>
									@endforeach
								</select>
							</div>
							<input type="submit" class="btn btn-primary btn-block" value="Save"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
