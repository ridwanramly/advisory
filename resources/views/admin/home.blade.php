@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-12">
			<table class="table table-light">
				<thead>
					<th>ID</th>
					<th>List name</th>
					<th>Distance</th>
					<th>User</th>
					<th>Action</th>
				</thead>
				<tbody>
					@foreach($listings as $list)
					<tr>
						<td>{{$list->id}}</td>
						<td>{{$list->list_name}}</td>
						<td>{{$list->distance}}</td>
						<td>{{$list->user->email}}</td>
						<td>
							<a href="{{route('deleteList', ['id' => $list->id])}}">Delete</a>
							<a href="{{route('viewList', ['id' => $list->id])}}">Edit</a>
						</td>
					</tr>

					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-4 col-12">
			<div class="card">
				<div class="card-header">
					Add New Listing
				</div>
				<div class="card-body">
					@if(session('status'))
						<div class="alert alert-success">
							{{session('status')}}
						</div>
					@endif
					<form method="POST" action="{{route('createList')}}">
						 @csrf
						<div class="form-group">
							<label>List Name</label>
							<input type="text" name="list_name" class="form-control">
						</div>
						<div class="form-group">
							<label>Distance</label>
							<input type="text" name="distance" class="form-control">
						</div>
						<div class="form-group">
							<label>User</label>
							<select class="form-control" name="user">
								@foreach($users as $user)
									<option value="{{$user->id}}">{{$user->email}}</option>
								@endforeach
							</select>
						</div>
						<input type="submit" class="btn btn-primary btn-block" value="Save"/>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
