<?php

namespace App\Http\Controllers;

use App\Listing;
use App\User;
use Hash;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $token;
    public function __construct()
    {
        $this->token = session('token');
    }
    public function index()
    {
        if (session('token')) {
            # code...
            return redirect('/admin');
        }
        return view('admin.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (Hash::check($request->password, $user->encrypted_password)) {
            # code...
            if ($user->type == 'a') {
                # code...
                $token = self::generateRandomString(32);
                $user->update([
                    'token' => $token,
                ]);
                session(['token' => $token]);
                return redirect('admin');

            }

            return redirect('/')->with('message', 'You u dont have access to this area');
        }

        return redirect('/')->with('message', 'Email or password invalid');
    }
    public function dashboard()
    {
        if (session('token')) {
            # code...
            $listings = Listing::all();
            $users    = User::where('type', 'u')->get();
            return view('admin.home', compact('listings', 'users'));
        }

        return redirect('/')->with('message', 'You dont have access');
    }
    public function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

}
