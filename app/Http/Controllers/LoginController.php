<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegisterRequest;
use App\User;
use Hash;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (Hash::check($request->password, $user->encrypted_password)) {
            $token = self::generateRandomString(32);
            $user->update([
                'token' => $token,
            ]);
            return response()->json([
                'id'     => $user->id,
                'token'  => $token,
                'status' => [
                    'code'    => 200,
                    'message' => 'Access Granted',
                ],
            ]);
        } else {
            return response()->json([
                'status' => [
                    'code'    => 505,
                    'message' => 'Wrong password and email',
                ],
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(UserRegisterRequest $request)
    {
        $user  = new User;
        $store = $user->create([
            'email'              => $request->email,
            'encrypted_password' => brcypt($request->password),
            'type'               => 'a',
        ]);

        return response()->json([
            'staus' => [
                'code'    => 200,
                'message' => 'success',
            ],
        ]);
    }
    public function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
