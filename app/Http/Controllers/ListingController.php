<?php

namespace App\Http\Controllers;

use App\Listing;
use App\User;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (session('token')) {
            # code...
            $list = new Listing;

            $store = $list->create([
                'list_name' => $request->list_name,
                'distance'  => $request->distance,
                'user_id'   => $request->user,
            ]);

            return redirect('/admin')->with('status', 'Saved');
        }

        return redirect('/')->with('message', 'You dont have access');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = User::where('token', request()->token)->first();
        if ($user != null) {
            $list = User::find(request()->id);
            return response()->json([
                'listng' => $list->listing,
                'status' => [
                    'code'    => '200',
                    'message' => 'Listing succesfully retrieved',
                ],
            ]);
        }
        return response()->json([
            'status' => [
                'code'    => '505',
                'message' => 'unauthicated',
            ],
        ]);
    }
    public function view($id)
    {
        $listing = Listing::find($id);
        $users   = User::where('type', 'u')->get();
        return view('admin.view', compact('listing', 'users'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (session('token') || request()->token) {
            $list   = Listing::find($request->id);
            $update = $list->update([
                'list_name' => $request->list_name,
                'distance'  => $request->distance,
                'user_id'   => $request->user,
            ]);

            return redirect('/admin')->with('update', 'Saved');
        }

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session('token')) {
            # code...
            $list   = Listing::find($id);
            $delete = $list->delete();

            return redirect('/')->with('detele', 'Deleted');
        }

        return redirect('/');
    }
}
