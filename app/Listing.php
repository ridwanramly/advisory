<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    protected $table    = 'listings';
    protected $fillable = [
        'id',
        'list_name',
        'distance',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
