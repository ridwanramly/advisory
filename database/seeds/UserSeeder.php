<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'email'              => 'admin@test.com',
                    'encrypted_password' => Hash::make('admin123'),
                    'type'               => 'a',
                ],
                [
                    'email'              => 'user1@test.com',
                    'encrypted_password' => Hash::make('user123'),
                    'type'               => 'u',

                ],
                [
                    'email'              => 'user2@test.com',
                    'encrypted_password' => Hash::make('user123'),
                    'type'               => 'u',

                ],
            ]
        );
    }
}
